#!/bin/bash

tlmgr option repository ftp://tug.org/historic/systems/texlive/2021/tlnet-final  #The CTAN repos are already using 2022 edition of texlive, but the container is not yet upgraded, so we use the legacy repos
tlmgr update --self
tlmgr install \
    filecontents \
    bytefield \
    standalone \
    pstricks \
    pst-tree \
    pst-node \
    xstring \
    preview \
    listingsutf8 \
    contour \
    tcolorbox \
    scalerel \
    xcolor \
    listofitems \
    contour \
    pgfplots \  
