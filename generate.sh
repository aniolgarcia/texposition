#!/bin/bash

BASEDIR=${PWD}


mkdir -p ./public/out/
mkdir -p ./public/img/
mkdir -p ./public/pdf/

mv static/ public/static/
mv tex/ public/tex/
cp -r public/tex/lib/ public/out/

cd public/

cat <<- _EOF > ./index.html
	<!--?xml version="1.0" encoding="utf-8"?-->
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
		<head>
			<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="./static/style.css">
			<title>TeX Gallery</title>
		</head>

		<body>
			<div id="navigation">
				<a accesskey="h" href="index.html"> AMUNT</a>
				|
				<a accesskey="H" href="https://ginkgo.cat/~aniol/"> INICI </a>
			</div>
			<h1><span class="tex">T<sub>e</sub>X</span><span style="color:#FF6666">position</span></h1>
			<main id="table">


	_EOF



figures=($(ls ./tex/*.tex))
for figure in ${figures[@]}
do
	[ -f "$figure" ] || break
	name=$(basename "${figure%.*}")
	pdflatex -interaction=nonstopmode -output-directory="./out" $figure
    while grep 'Rerun to get ' ./out/$name.log
    do 
        pdflatex -interaction=batchmode -output-directory="./out" $figure
    done
    pdftoppm -png -r 600 ./out/$name.pdf > ./img/$name.png 
	mv ./out/$name.pdf ./pdf

	cat <<- _EOF > ./$name.html
	<!--?xml version="1.0" encoding="utf-8"?-->
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
		<head>
			<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="./static/style.css">
			<title>$name</title>
		</head>

		<body>
			<div id="navigation">
				<a accesskey="h" href="index.html"> AMUNT</a>
				|
				<a accesskey="H" href="https://ginkgo.cat/~aniol/"> INICI </a>
			</div>
			<main>
				<h1>$name</h1>
				<img src="./img/$name.png" style="max-width: 800px;">
				<pre><code>
					$(cat $figure)
				</pre></code>
			</main>
		</body>
	</html>
	_EOF



	cat <<- _EOF >> ./index.html
				<div class="cell">
					<a href="$name.html">
						<img src="./img/$name.png" style="max-width:218px; max-height:218px">
					</a>
					<div class="container">
						[<a href="./pdf/$name.pdf">PDF</a>]
						[<a href="./tex/$name.tex">TEX</a>]
						
						<a href="$name.html"><br><b>$name</b></a>
					</div>
				</div>
	_EOF
done

cat << _EOF >> ./index.html
			</main>
		</body>
	</html>
_EOF
